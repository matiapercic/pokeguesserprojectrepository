import liquibase.pro.packaged.P;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PokemonRowMapper implements RowMapper{


    @Override
    public Pokemon mapRow(ResultSet rs, int rowNum) throws SQLException {

        Pokemon pokemon = new Pokemon();

        pokemon.setID(rs.getInt("ID"));
        pokemon.setDex_Number(rs.getInt("Dex_Number"));
        pokemon.setName(rs.getString("Name"));
        pokemon.setType_One_ID(rs.getInt("Type_One_ID"));
        pokemon.setType_Two_ID(rs.getInt("Type_Two_ID"));
        pokemon.setDominant_Colour_One_ID(rs.getInt("Dominant_Colour_One_ID"));
        pokemon.setDominant_Colour_Two_ID(rs.getInt("Dominant_Colour_Two_ID"));
        pokemon.setStage_Number(rs.getInt("Stage_Number"));
        pokemon.setDex_Entry(rs.getString("Dex_Entry"));
        pokemon.setEvolution_Method_ID(rs.getInt("Evolution_Method_ID"));
        pokemon.setForm_ID(rs.getInt("Form_ID"));
        pokemon.setBase_Stat_Total(rs.getInt("Base_Stat_Total"));


        return pokemon;
    }
}
