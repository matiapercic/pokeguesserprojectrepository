import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EvolutionRowMapper implements RowMapper{
    @Override
    public EvolutionMethod mapRow(ResultSet rs, int rowNum) throws SQLException {

        EvolutionMethod evolution_method=new EvolutionMethod();

        evolution_method.setID_Evolution_Method(rs.getInt("ID_Evolution_Method"));
        evolution_method.setEvolution_Method_Name(rs.getString("Evolution_Method_Name"));

        return evolution_method;
    }
}
