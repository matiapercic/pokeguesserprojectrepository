import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColourOneRowMapper implements RowMapper{
    @Override
    public DomColourOne mapRow(ResultSet rs, int rowNum) throws SQLException {

        DomColourOne colour_one=new DomColourOne();

        colour_one.setID_Dominant_Colour_One(rs.getInt("ID_Dominant_Colour_One"));
        colour_one.setColour_Name(rs.getString("Colour_Name"));

        return colour_one;
    }
}
