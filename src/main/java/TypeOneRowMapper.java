
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TypeOneRowMapper implements  RowMapper{


    @Override
    public TypeOne mapRow(ResultSet rs, int rowNum) throws SQLException {

        TypeOne attribute_type_one = new TypeOne();

        attribute_type_one.setID_Type_One(rs.getInt("ID_Type_One"));
        attribute_type_one.setType_Name(rs.getString("Type_Name"));

        return attribute_type_one;
    }
}
