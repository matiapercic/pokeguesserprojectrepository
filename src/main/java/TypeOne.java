public class TypeOne {

    private int ID_Type_One;
    private String Type_Name;

    public int getID_Type_One() {
        return ID_Type_One;
    }

    public void setID_Type_One(int ID_Type_One) {
        this.ID_Type_One = ID_Type_One;
    }

    public String getType_Name() {
        return Type_Name;
    }

    public void setType_Name(String type_Name) {
        Type_Name = type_Name;
    }

    @Override
    public String toString() {
        return "AttributeTypeOne{" +
                "ID_Type_One=" + ID_Type_One +
                ", Type_Name='" + Type_Name + '\'' +
                '}';
    }
}
