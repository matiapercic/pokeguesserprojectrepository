public class DomColourTwo {
    private int ID_Dominant_Colour_Two;
    private String Colour_Name;

    public int getID_Dominant_Colour_Two() {
        return ID_Dominant_Colour_Two;
    }

    public void setID_Dominant_Colour_Two(int ID_Dominant_Colour_Two) {
        this.ID_Dominant_Colour_Two = ID_Dominant_Colour_Two;
    }

    public String getColour_Name() {
        return Colour_Name;
    }

    public void setColour_Name(String colour_Name) {
        Colour_Name = colour_Name;
    }

    @Override
    public String toString() {
        return "DomColourTwo{" +
                "ID_Dominant_Colour_Two=" + ID_Dominant_Colour_Two +
                ", Colour_Name='" + Colour_Name + '\'' +
                '}';
    }
}
