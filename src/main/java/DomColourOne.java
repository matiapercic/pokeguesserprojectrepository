public class DomColourOne {

    private int ID_Dominant_Colour_One;
    private String Colour_Name;

    public int getID_Dominant_Colour_One() {
        return ID_Dominant_Colour_One;
    }

    public void setID_Dominant_Colour_One(int ID_Dominant_Colour_One) {
        this.ID_Dominant_Colour_One = ID_Dominant_Colour_One;
    }

    public String getColour_Name() {
        return Colour_Name;
    }

    public void setColour_Name(String colour_Name) {
        Colour_Name = colour_Name;
    }

    @Override
    public String toString() {
        return "DomColourOne{" +
                "ID_Dominant_Colour_One=" + ID_Dominant_Colour_One +
                ", Colour_Name='" + Colour_Name + '\'' +
                '}';
    }
}
