import java.util.List;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;




public class ConnectionCheck {
    public static void main(String[] args) {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        dataSource.setUrl("jdbc:sqlserver://localhost:1433;;database=PokeGuesserProject;");
        dataSource.setUsername("rpuser");
        dataSource.setPassword("24601");

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String sql = "SELECT * FROM dbo.Pokemon";

        List<Pokemon> allpokemon = jdbcTemplate.query(sql, new PokemonRowMapper());


        for (Pokemon pokemon : allpokemon)
            System.out.println(pokemon);
    }
}
