public class Form {
    private int ID_Form;
    private String Form_Name;

    public int getID_Form() {
        return ID_Form;
    }

    public void setID_Form(int ID_Form) {
        this.ID_Form = ID_Form;
    }

    public String getForm_Name() {
        return Form_Name;
    }

    public void setForm_Name(String form_Name) {
        Form_Name = form_Name;
    }

    @Override
    public String toString() {
        return "Form{" +
                "ID_Form=" + ID_Form +
                ", Form_Name='" + Form_Name + '\'' +
                '}';
    }
}
