public class TypeTwo {

    private int ID_Type_Two;
    private String Type_Name;

    public int getID_Type_Two() {
        return ID_Type_Two;
    }

    public void setID_Type_Two(int ID_Type_Two) {
        this.ID_Type_Two = ID_Type_Two;
    }

    public String getType_Name() {
        return Type_Name;
    }

    public void setType_Name(String type_Name) {
        Type_Name = type_Name;
    }

    @Override
    public String toString() {
        return "AttributeTypeTwo{" +
                "ID_Type_Two=" + ID_Type_Two +
                ", Type_Name='" + Type_Name + '\'' +
                '}';
    }
}
