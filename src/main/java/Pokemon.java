public class Pokemon {

private int ID;
private int Dex_Number;
private String Name;
private int Type_One_ID;
private int Type_Two_ID;
private int Dominant_Colour_One_ID;
private int Dominant_Colour_Two_ID;
private int Stage_Number;
private String Dex_Entry;
private int Evolution_Method_ID;
private int Form_ID;
private int Base_Stat_Total;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getDex_Number() {
        return Dex_Number;
    }

    public void setDex_Number(int dex_Number) {
        Dex_Number = dex_Number;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getType_One_ID() {
        return Type_One_ID;
    }

    public void setType_One_ID(int type_One_ID) {
        Type_One_ID = type_One_ID;
    }

    public int getType_Two_ID() {
        return Type_Two_ID;
    }

    public void setType_Two_ID(int type_Two_ID) {
        Type_Two_ID = type_Two_ID;
    }

    public int getDominant_Colour_One_ID() {
        return Dominant_Colour_One_ID;
    }

    public void setDominant_Colour_One_ID(int dominant_Colour_One_ID) {
        Dominant_Colour_One_ID = dominant_Colour_One_ID;
    }

    public int getDominant_Colour_Two_ID() {
        return Dominant_Colour_Two_ID;
    }

    public void setDominant_Colour_Two_ID(int dominant_Colour_Two_ID) {
        Dominant_Colour_Two_ID = dominant_Colour_Two_ID;
    }

    public int getStage_Number() {
        return Stage_Number;
    }

    public void setStage_Number(int stage_Number) {
        Stage_Number = stage_Number;
    }

    public String getDex_Entry() {
        return Dex_Entry;
    }

    public void setDex_Entry(String dex_Entry) {
        Dex_Entry = dex_Entry;
    }

    public int getEvolution_Method_ID() {
        return Evolution_Method_ID;
    }

    public void setEvolution_Method_ID(int evolution_Method_ID) {
        Evolution_Method_ID = evolution_Method_ID;
    }

    public int getForm_ID() {
        return Form_ID;
    }

    public void setForm_ID(int form_ID) {
        Form_ID = form_ID;
    }

    public int getBase_Stat_Total() {
        return Base_Stat_Total;
    }

    public void setBase_Stat_Total(int base_Stat_Total) {
        Base_Stat_Total = base_Stat_Total;
    }

    @Override
    public String toString() {
        return "{" +
                ", ID=" + ID +
                ", Dex_Number=" + Dex_Number +
                ", Name='" + Name + '\'' +
                ", Type_One_ID=" + Type_One_ID +
                ", Type_Two_ID=" + Type_Two_ID +
                ", Dominant_Colour_One_ID=" + Dominant_Colour_One_ID +
                ", Dominant_Colour_Two_ID=" + Dominant_Colour_Two_ID +
                ", Stage_Number=" + Stage_Number +
                ", Dex_Entry='" + Dex_Entry + '\'' +
                ", Evolution_Method_ID=" + Evolution_Method_ID +
                ", Form_ID=" + Form_ID +
                ", Base_Stat_Total=" + Base_Stat_Total +
                '}';
    }
}
