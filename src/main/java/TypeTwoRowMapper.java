import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TypeTwoRowMapper implements RowMapper {
    @Override
    public TypeTwo mapRow(ResultSet rs, int rowNum) throws SQLException {

        TypeTwo attribute_type_two= new TypeTwo();

        attribute_type_two.setID_Type_Two(rs.getInt("ID_Type_Two"));
        attribute_type_two.setType_Name(rs.getString("Type_Name"));

        return attribute_type_two;

    }
}
