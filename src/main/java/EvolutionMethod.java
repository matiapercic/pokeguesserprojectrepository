public class EvolutionMethod {

    private int ID_Evolution_Method;
    private String Evolution_Method_Name;

    public int getID_Evolution_Method() {
        return ID_Evolution_Method;
    }

    public void setID_Evolution_Method(int ID_Evolution_Method) {
        this.ID_Evolution_Method = ID_Evolution_Method;
    }

    public String getEvolution_Method_Name() {
        return Evolution_Method_Name;
    }

    public void setEvolution_Method_Name(String evolution_Method_Name) {
        Evolution_Method_Name = evolution_Method_Name;
    }

    @Override
    public String toString() {
        return "EvolutionMethod{" +
                "ID_Evolution_Method=" + ID_Evolution_Method +
                ", Evolution_Method_Name='" + Evolution_Method_Name + '\'' +
                '}';
    }
}
