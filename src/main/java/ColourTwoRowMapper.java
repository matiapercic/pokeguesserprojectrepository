import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColourTwoRowMapper implements  RowMapper{
    @Override
    public DomColourTwo mapRow(ResultSet rs, int rowNum) throws SQLException {

        DomColourTwo colour_two=new DomColourTwo();

        colour_two.setID_Dominant_Colour_Two(rs.getInt("ID_Dominant_Colour_Two"));
        colour_two.setColour_Name(rs.getString("Colour_Name"));

        return colour_two;
    }
}
