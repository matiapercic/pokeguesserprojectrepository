import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FormRowMapper implements RowMapper{
    @Override
    public Form mapRow(ResultSet rs, int rowNum) throws SQLException {

        Form form=new Form();

        form.setID_Form(rs.getInt("ID_Form"));
        form.setForm_Name(rs.getString("Form_Name"));

        return form;
    }
}
